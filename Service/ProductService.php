<?php
namespace BorysZielonka\ApiStoreProductBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Exception\HttpException;
use BorysZielonka\ApiStoreProductBundle\Entity\Product;

/**
 * 
 */
class ProductService
{

    const BUNDLE_CLASS_NAME = "BorysZielonkaApiStoreProductBundle:Product";

    /**
     *
     * @var Doctrine\ORM\EntityManager 
     */
    protected $entityManager;

    /**
     * 
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * 
     * @param type $moreThanAmount
     * @param type $inStock
     * @return type
     * @return array
     */
    public function findProductList($minimumAmount, $inStock)
    {
        $productRepo = $this->entityManager->getRepository(self::BUNDLE_CLASS_NAME);
        $products = $productRepo->getProductList($minimumAmount, $inStock);

        return $products;
    }

    /**
     * 
     * @param type $id
     * @return type
     * @return array
     */
    public function findProductById($id)
    {
        $productRepo = $this->entityManager->getRepository(self::BUNDLE_CLASS_NAME);
        $product = $productRepo->find($id);

        return $product;
    }

    /**
     * 
     * @param type $name
     * @param type $amount
     * @throws HttpException
     * @return bool
     */
    public function createProduct($name, $amount)
    {
        $product = new Product();
        $product->setName($name);
        $product->setAmount($amount);

        $this->entityManager->persist($product);
        $this->entityManager->flush();

        return true;
    }

    /**
     * 
     * @param type $productName
     * @param type $productAmount
     * @param type $id
     * @throws HttpException
     * @return bool
     */
    public function updateProduct($productName, $productAmount, $id)
    {
        $em = $this->entityManager;
        $productRepo = $em->getRepository(self::BUNDLE_CLASS_NAME);
        $product = $productRepo->find($id);

        if (!$product) {
            return false;
        }

        if (isset($productName)) {
            $product->setName($productName);
        }
        if (isset($productAmount)) {
            $product->setAmount($productAmount);
        }

        $em->flush();

        return true;
    }

    /**
     * 
     * @param type $id
     * @throws HttpException
     * @return bool
     */
    public function deleteProduct($id)
    {
        $em = $this->entityManager;
        $productRepo = $em->getRepository(self::BUNDLE_CLASS_NAME);
        $product = $productRepo->find($id);

        if (!$product) {
            return false;
        }

        $em->remove($product);
        $em->flush();

        return true;
    }
}
