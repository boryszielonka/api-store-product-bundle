<?php
namespace BorysZielonka\ApiStoreProductBundle\Controller;

use BorysZielonka\ApiStoreProductBundle\Service\ProductService;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ProductController extends FOSRestController
{

    /**
     *
     * @Rest\Get("api/product/")
     * @return View | $products
     * @throws HttpException
     */
    public function listAction(Request $request)
    {
        $minimumAmount = $request->get('minimumAmount');
        $inStock = $request->get('inStock');

        if (isset($minimumAmount) && isset($inStock) &&
            $minimumAmount > 0 && $inStock < 1) {
            throw new HttpException(400, "Invalid parameters");
        }

        $productService = $this->get(ProductService::class);
        $products = $productService->findProductList($minimumAmount, $inStock);
        return $products;
    }

    /**
     * 
     * @Rest\Get("api/product/{id}")
     * @param type $id
     * @return type
     * @throws HttpException
     */
    public function getAction($id)
    {
        $productService = $this->get(ProductService::class);
        $product = $productService->findProductById($id);

        if (!$product) {
            throw new HttpException(404, 'No product found for id ' . $id);
        }

        return $product;
    }

    /**
     * 
     * @Rest\Post("api/product/")
     * @param Request $request
     * @return View
     * @throws HttpException
     */
    public function createAction(Request $request)
    {
        $name = $request->get('name');
        $amount = $request->get('amount');

        if (empty($name) || $amount == NULL) {
            throw new HttpException(400, 'At least one parameter is empty.');
        }

        if (!preg_match('/^\d+$/', $amount)) {
            throw new HttpException(400, 'Invalid amount. Use digits only.');
        }

        $productService = $this->get(ProductService::class);
        $productService->createProduct($name, $amount);

        return new View("Product created", Response::HTTP_OK);
    }

    /**
     * 
     * 
     * @Rest\Put("api/product/{id}")
     * @param Request $request
     * @param type $id
     * @return View
     * @throws HttpException
     */
    public function updateAction(Request $request, $id)
    {
        $productName = $request->get('name');
        $productAmount = $request->get('amount');

        if (empty($productName) && empty($productAmount)) {
            throw new HttpException(400, 'Nothing to update for product ' . $id);
        }

        $productService = $this->get(ProductService::class);

        if (!$productService->updateProduct($productName, $productAmount, $id)) {
            return new View("Product " . $id . " update failed", Response::HTTP_BAD_REQUEST);
        }

        return new View("Product " . $id . " updated", Response::HTTP_OK);
    }

    /**
     * 
     * @Rest\Delete("api/product/{id}")
     * @param type $id
     * @return View
     */
    public function deleteAction($id)
    {
        $productService = $this->get(ProductService::class);

        if (!$productService->deleteProduct($id)) {
            return new View("Product " . $id . " delete failed", Response::HTTP_BAD_REQUEST);
        }

        return new View("Product " . $id . " deleted", Response::HTTP_OK);
    }
}
