Bundle compatible with Symfony 3.3.*

## 1
composer.json
```json
    ...
    "repositories" : [
        {
        "type" : "vcs",
        "url" : "https://gitlab.com/boryszielonka/api-store-product-bundle.git"
        }
    ],
    ...
    "config": {
            "platform": {
                "php": "7"
            },
            "sort-packages": true
        },
```

```sh
composer require friendsofsymfony/rest-bundle &&
composer require jms/serializer-bundle &&
composer require nelmio/cors-bundle &&
composer require boryszielonka/api-store-product-bundle dev-master &&
composer require --dev doctrine/doctrine-fixtures-bundle &&
composer require --dev phpunit/phpunit 5.6 &&
composer require --dev symfony/phpunit-bridge ^3.0
```

## 2
app/AppKernel.php
```php
// registerBundles()
$bundles = [
// ...
    new BorysZielonka\ApiStoreProductBundle\BorysZielonkaApiStoreProductBundle(),
    new FOS\RestBundle\FOSRestBundle(),
    new Nelmio\CorsBundle\NelmioCorsBundle(),
    new JMS\SerializerBundle\JMSSerializerBundle()
];

if (in_array($this->getEnvironment(), ['dev', 'test'], true)) {
// ...
    $bundles[] = new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle();

```

## 3
app/config/routing.yml
```yml
    borys_zielonka_api_store_product:
        resource: "@BorysZielonkaApiStoreProductBundle/Controller/"
        type:     annotation
        prefix:   /
```

## 4
app/config/config.yml
```yml
fos_rest:
    body_listener: true
    format_listener:
        rules:
            - { path: '^/', priorities: ['json'], fallback_format: json, prefer_extension: false }
    param_fetcher_listener: true
    view:
        view_response_listener: 'force'
        formats:
            json: true

nelmio_cors:
    defaults:
        allow_credentials: false
        allow_origin: ['*']
        allow_headers: ['*']
        allow_methods: ['GET', 'POST', 'PUT', 'DELETE']
        max_age: 0
        hosts: []
        origin_regex: false
        forced_allow_origin_value: ~
```


## 5
```
bin/console doctrine:database:create &&
bin/console doctrine:schema:create
```

INSERT records to database
```sh
bin/console doctrine:fixtures:load
```
or

```sql
bin/console doctrine:query:sql "INSERT INTO product (name, amount) VALUES ('Produkt 1',  4)" &&
bin/console doctrine:query:sql "INSERT INTO product (name, amount) VALUES ('Produkt 2',  12)" &&
bin/console doctrine:query:sql "INSERT INTO product (name, amount) VALUES ('Produkt 5',  0)" &&
bin/console doctrine:query:sql "INSERT INTO product (name, amount) VALUES ('Produkt 7',  6)" &&
bin/console doctrine:query:sql "INSERT INTO product (name, amount) VALUES ('Produkt 8',  2)"
```

